$(function(){
	
	$('#finishFlag').css("display", "none")
	$('#countdown').css("display", "none")
	$('#track').css("background-image", "url(./img/raceTrack.jpg)")
	let calculatedMargin = $('#track').width() - $('.cars').width()
	let car1 = 'Car 1'
	let car2 = 'Car 2'
	$('#raceBtn').on('click', function (e) {
		e.preventDefault()
		let counter = 4
		let interval = setInterval(function(){
		counter--	
		$('#countdown').css("display","inline-block")
		$('#countdown').text(counter)
		$('#track').css("background-image", "linear-gradient(rgba(0, 0, 0, 0.527),rgba(0, 0, 0, 0.5)),url(./img/raceTrack.jpg)")
			if(counter === 0){
				function checkIfComplete(){
					if(isComplete == false){
					isComplete = true
					$('.btn').prop('disabled', false)
					}else {
					place = 'second'
					}
				}
				let isComplete = false
				clearInterval(interval)
				$('#countdown').css("display", "none")
				$('#track').css("background-image", "url(./img/raceTrack.jpg)")
				let minSpeed = 1000
				let car1Speed = parseInt(minSpeed + Math.random()*2000, 10)
				let car2Speed = parseInt(minSpeed + Math.random()*2000, 10)
				let place = 'first'
				
				$('.carOne').animate({
					marginLeft: calculatedMargin
				}, car1Speed, function(){
					checkIfComplete()
					$('.statsCar1').append(`<div class="w-50 mx-auto text-secondary border">Finished in: <span class="text-white font-weight-bolder">${place}</span> place with a time of: <span class="text-white font-weight-bolder">${car1Speed}</span> milliseconds!</div>`)
					localStorage.setItem("car1Place", place)
					localStorage.setItem("speed1", car1Speed)
					if(isComplete == true && car1Speed < car2Speed){
						$('#finishFlag').css("display", "block")
						$('#track').css("background-image", "linear-gradient(rgba(0, 0, 0, 0.527),rgba(0, 0, 0, 0.5)),url(./img/raceTrack.jpg)")
					}
				})
				$('.carTwo').animate({
					marginLeft: calculatedMargin
				}, car2Speed, function(){
					checkIfComplete()
					$('.statsCar2').append(`<div class="w-50 mx-auto text-secondary border">Finished in: <span class="text-danger font-weight-bolder">${place}</span> place with a time of: <span class="text-danger font-weight-bolder">${car2Speed}</span> milliseconds!</div>`)
					localStorage.setItem("car2Place", place)
					localStorage.setItem("speed2", car2Speed)
					if(isComplete == true && car2Speed < car1Speed){
						$('#finishFlag').css("display", "block")
						$('#track').css("background-image", "linear-gradient(rgba(0, 0, 0, 0.527),rgba(0, 0, 0, 0.5)),url(./img/raceTrack.jpg)")
					}
				})
				$('.btn').prop('disabled', true)
			}
		}, 1000)
	})
	$('#startBtn').on('click', function (e) {
		e.preventDefault()
		$('#finishFlag').css("display", "none")
		$('#track').css("background-image", "url(./img/raceTrack.jpg)")
		$('.cars').css({marginLeft: 0})
	})
	let checkLocalStorage = localStorage.getItem("speed1")
	if(checkLocalStorage){
		$('#previousResults')
		.html(`<h3>Results from previous time you played this game:</h3>
		<div class="text-secondary border w-50 mx-auto py-2"><span class="text-white font-weight-bolder">${car1}</span> finished in <span class="text-white font-weight-bolder">${localStorage.getItem("car1Place")}</span> place, with a time of <span class="text-white font-weight-bolder">${localStorage.getItem("speed1")}</span> milliseconds!</div>
		<div class="text-secondary border w-50 mx-auto py-2"><span class="text-danger font-weight-bolder">${car2}</span> finished in <span class="text-danger font-weight-bolder">${localStorage.getItem("car2Place")}</span> place, with a time of <span class="text-danger font-weight-bolder">${localStorage.getItem("speed2")}</span> milliseconds!</div>`)
	}
});